package junior.databases.homework;

import java.util.*;
import java.sql.*;
import java.lang.reflect.Constructor;

class ModifiedException extends RuntimeException {};

public abstract class Entity {
    private static String DELETE_QUERY   = "DELETE FROM \"%1$s\" WHERE %1$s_id=?";
    private static String INSERT_QUERY   = "INSERT INTO \"%1$s\" (%2$s) VALUES (%3$s) RETURNING %1$s_id";
    private static String LIST_QUERY     = "SELECT * FROM \"%s\"";
    private static String SELECT_QUERY   = "SELECT * FROM \"%1$s\" WHERE %1$s_id=?";
    private static String CHILDREN_QUERY = "SELECT * FROM \"%1$s\" WHERE %2$s_id=?";
    private static String SIBLINGS_QUERY = "SELECT * FROM \"%1$s\" NATURAL JOIN \"%2$s\" WHERE %3$s_id=?";
    private static String UPDATE_QUERY   = "UPDATE \"%1$s\" SET %2$s WHERE %1$s_id=?";

    private static Connection db = null;

    protected boolean isLoaded = false;
    protected boolean isModified = false;
    private String table = null;
    private int id = 0;
    protected Map<String, Object> fields = new HashMap<>();

    public Entity() {
        this.table = this.getClass().getSimpleName().toLowerCase();
    }

    public Entity(Integer id) {
        this();
        this.id = id;
    }

    public static final void setDatabase(Connection connection)  {
        if (connection == null) {
            throw new NullPointerException();
        }
        Entity.db = connection;
    }

    public final int getId() {
        return (int)getColumn("id");
    }

    public final java.util.Date getCreated() {
        return getDate("created");
    }

    public final java.util.Date getUpdated() {
        return getDate("updated");
    }

    private java.util.Date getDate(String column) {
        int value = (int) this.getColumn(column);

        return new java.util.Date((long)value * 1000L);
    }

    public final Object getColumn(String name) {
        return getColumn(this.table, name);
    }

    public final Object getColumn(String table, String column) {
        if (this.isModified) {
            throw new ModifiedException();
        }

        this.load();

        String columnName = getColumnName(table, column);
        if (!this.fields.containsKey(columnName)) {
            throw new IllegalArgumentException();
        }
        return this.fields.get(columnName);
    }

    private String getColumnName(String table, String column) {
        return table + "_" + column;
    }

    public final <T extends Entity> T getParent(Class<T> cls) {
        // get parent id from fields as <classname>_id, create and return an instance of class T with that id
        String table = cls.getSimpleName().toLowerCase();
        int id = 0;

        try {
            id = (int) getColumn(table, "id");
            Constructor<T> constructor = cls.getDeclaredConstructor(Integer.class);

            return constructor.newInstance(id);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    public final <T extends Entity> List<T> getChildren(Class<T> cls) {
        // select needed rows and ALL columns from corresponding table
        // convert each row from ResultSet to instance of class T with appropriate id
        // fill each of new instances with column data
        return getRows(
                cls,
                CHILDREN_QUERY,
                cls.getSimpleName().toLowerCase(),
                this.table
        );
    }

    public final <T extends Entity> List<T> getSiblings(Class<T> cls) {
        // select needed rows and ALL columns from corresponding table
        // convert each row from ResultSet to instance of class T with appropriate id
        // fill each of new instances with column data
        // return list of sibling instances
        String clsName = cls.getSimpleName().toLowerCase();
        return getRows(
                cls,
                SIBLINGS_QUERY,
                clsName,
                getJoinTableName(clsName, this.table),
                this.table
        );
    }

    public final void setColumn(String name, Object value) {
        // put a value into fields with <table>_<name> as a key
        setColumn(this.table, name, value);
    }

    private void setColumn(String table, String name, Object value) {
        // put a value into fields with <table>_<name> as a key
        String columnName = getColumnName(table, name);

        this.fields.put(columnName, value);
        this.isModified = true;
    }

    public final void setParent(String name, Integer id) {
        // put parent id into fields with <name>_<id> as a key
        setColumn(name, "id", id);
    }

    private void load() {
        // check, if current object is already loaded
        // get a single row from corresponding table by id
        // store columns as object fields with unchanged column names as keys
        if (this.isLoaded) {
            return;
        }

        try ( PreparedStatement stmt =
                      db.prepareStatement(String.format(SELECT_QUERY, this.table))
        ) {
            stmt.setInt(1, this.id);

            ResultSet rs = stmt.executeQuery();
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            if (rs.next()) {
                for(int i=1; i<=columns; i++){
                    fields.put(md.getColumnName(i), rs.getObject(i));
                }
            }

            this.isLoaded = true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void insert() {
        // execute an insert query, built from fields keys and values
        try (PreparedStatement stmt = db.prepareStatement(String.format(
                    INSERT_QUERY,
                    this.table,
                    join(this.fields.keySet()),
                    join(genPlaceholders(this.fields.size()))
        ))) {
            bindValuesToQuery(stmt);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            this.id = rs.getInt(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void update() {
        // execute an update query, built from fields keys and values
        try (PreparedStatement stmt = db.prepareStatement(String.format(
                UPDATE_QUERY,
                this.table,
                String.format("%s=?", join(this.fields.keySet(), "=?, "))
        ))) {
            bindValuesToQuery(stmt);
            stmt.setInt(this.fields.size() + 1, this.id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void bindValuesToQuery(PreparedStatement stmt) {
        Collection values = this.fields.values();
        Iterator it = values.iterator();

        for (int i = 1; it.hasNext(); i++ ) {
            try {
                stmt.setObject(i, it.next());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public final void delete() {
        // execute a delete query with current instance id
        try (PreparedStatement stmt = db.prepareStatement(String.format(
                    DELETE_QUERY,
                    this.table
        ))) {
            stmt.setInt(1, this.id);
            stmt.execute();

            this.fields.clear();
            this.isLoaded = false;
            this.isModified = false;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public final void save() {
        // execute either insert or update query, depending on instance id
        if (this.id == 0) {
            insert();
        } else {
            update();
        }
        this.isModified = false;
    }

    protected static <T extends Entity> List<T> all(Class<T> cls) {
        // select ALL rows and ALL columns from corresponding table
        // convert each row from ResultSet to instance of class T with appropriate id
        // fill each of new instances with column data
        // aggregate all new instances into a single List<T> and return it
        try (PreparedStatement stmt = db.prepareStatement(String.format(
                    LIST_QUERY,
                    cls.getSimpleName().toLowerCase()
        ))) {
            ResultSet rows = stmt.executeQuery();

            return rowsToEntities(cls, rows);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private <T extends Entity> List<T> getRows(Class<T> cls, String query, Object... args) {
        try (PreparedStatement stmt = db.prepareStatement(String.format(
                query,
                args
        ))) {
            stmt.setInt(1, this.id);
            ResultSet rows = stmt.executeQuery();

            return rowsToEntities(cls, rows);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static Collection<String> genPlaceholders(int size) {
        // return a Collection<String>, consisting of <size> "?" symbols, joined with ", "
        // each "?" is used in insert statements as a placeholder for values (google prepared statements)
        return genPlaceholders(size, "?");
    }

    private static Collection<String> genPlaceholders(int size, String placeholder) {
        // return a Collection<String>, consisting of <size> <placeholder> symbols, joined with ", "
        // each <placeholder> is used in insert statements as a placeholder for values (google prepared statements)
        List<String> list = new ArrayList<>();

        for (int i = 0; i < size; i++) list.add(placeholder);
        return list;
    }

    private static String getJoinTableName(String leftTable, String rightTable) {
        // generate the name of associative table for many-to-many relation
        // sort left and right tables alphabetically
        // return table name using format <table>__<table>
        if ( leftTable.compareTo(rightTable) <= 0 ) {
            return leftTable + "__" + rightTable;
        }
        return rightTable + "__" + leftTable;
    }

    private static String join(Collection<String> sequence) {
        // join collection of strings with ", " as glue and return a joined string
        return join(sequence, ", ");
    }

    private static String join(Collection<String> sequence, String glue) {
        // join collection of strings with glue and return a joined string
        StringBuffer sb = new StringBuffer();
        Iterator<String> it = sequence.iterator();

        sb.append(it.next());
        for ( ;it.hasNext(); ) {
            sb.append(glue).append(it.next());
        }

        return sb.toString();
    }

    private static <T extends Entity> List<T> rowsToEntities(Class<T> cls, ResultSet rows) {
        // convert a ResultSet of database rows to list of instances of corresponding class
        // each instance must be filled with its data so that it must not produce additional queries to database to get it's fields
        List<T> list = new ArrayList<>();
        try {
            Constructor<T> constructor = cls.getDeclaredConstructor(Integer.class);
            ResultSetMetaData md = rows.getMetaData();
            int colSize = md.getColumnCount();
            
            while (rows.next()) {
                //? getDeclaredConstructor
                int id = rows.getInt(1);
                T entity = constructor.newInstance(id);


                for (int i = 1; i <= colSize; i++) {
                    entity.fields.put(md.getColumnName(i), rows.getObject(i));
                    entity.isLoaded = true;
                }

                list.add(entity);
            }
            return list;
        } catch (SQLException | ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }
}
