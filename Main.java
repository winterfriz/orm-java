package junior.databases.homework;

import java.sql.*;

public class Main {
    private static Connection connection = null;

    public static void main(String[] args) {
        initDatabase();

        Entity.setDatabase(connection);

//        Post post = new Post(1);
//
//        System.out.println(post.getId());
//        System.out.println(post.getCategory().getId());
//        System.out.println(post.getCreated());
//        System.out.println(post.getUpdated());
//        System.out.println(post.getColumn("title"));

//        post.setColumn("content", "Very interesting content");
//        post.setTitle("Strange title");
//        System.out.println(post.getTitle());
//        post.save();
//        post.delete();

//        User user = new User();
//
//        user.setEmail("00742fuck@gmail.com");
//        user.setName("hhhh");
//        user.save();

//        System.out.println(user.getId());
//        Tag tag = new Tag();
//
//        tag.setName("ASd");
//        tag.save();
//
//        System.out.println(tag.getName());
//        System.out.println(tag.getId());


//        for (Tag tag : Tag.all(Tag.class)) {
//            System.out.println(String.format("%d %s",
//                    tag.getId(),
//                    tag.getName()
//            ));
//        }

//        Tag tag = new Tag(4);
//
//        tag.delete();

//        Post post = new Post(1);
//        for (Tag tag : post.getTags()) {
//            System.out.println(String.format("%d %s %s",
//                    tag.getId(),
//                    tag.getName(),
//                    tag.getColumn("post","id")
//            ));
//        }
//

//        Tag tag = new Tag(2);
//        for ( Post post : tag.getPosts() ) {
//            System.out.println(String.format("%d %s",
//                    post.getId(),
//                    post.getContent()
//            ));
//        }


    }

    private static void initDatabase() {
        try {
            Class.forName("org.postgresql.Driver");

            connection = DriverManager.getConnection(
                        "jdbc:postgresql://localhost/orm", "postgres",
                        "asdw");
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}

